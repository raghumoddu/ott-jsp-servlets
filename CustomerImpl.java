package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Customer;
import util.Db;
import util.Query;

public class CustomerImpl implements ICustomer {
  PreparedStatement pst = null;
  ResultSet rs = null;
  int result = 0;

  @Override
  public int addCustomer(Customer customer) {
    try {
      pst = Db.getCon().prepareStatement(Query.addCustomer);
      pst.setString(1,customer.getName());
      pst.setString(2, customer.getMobile());
      pst.setString(3, customer.getAmount());
      pst.setDate(4, customer.getValidupto());
      result = pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      result = 0;
    } finally {
      try {
        Db.getCon().close();
        pst.close();
      } catch (ClassNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    return result;
  }

  @Override
  public List<Customer> viewCustomer() {
    List<Customer> customer = new ArrayList<Customer>();
    try {
      pst = Db.getCon().prepareStatement(Query.viewCustomer);
      rs = pst.executeQuery();
      while (rs.next()) {
    	  Customer c = new Customer(rs.getString(1), rs.getString(2), rs.getString(3), rs.getDate(4));
    	  customer.add(c);
      }

    } catch (ClassNotFoundException | SQLException e) {
      try {
        Db.getCon().close();
        pst.close();
        rs.close();
      } catch (ClassNotFoundException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      } catch (SQLException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      }

    }
    return customer;
  }

  @Override
  public int editCustomer(Customer customer) {
    try {
      pst = Db.getCon().prepareStatement(Query.editCustomer);
      pst.setString(4, customer.getMobile());
      pst.setString(1, customer.getName());
      pst.setString(2, customer.getAmount());
      pst.setDate(3, customer.getValidupto());
      result = pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      result = 0;
    } finally {
      try {
        Db.getCon().close();
        pst.close();
      } catch (ClassNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    return result;
  }

  @Override
  public int removeCustomer(Customer customer) {
    try {
      pst = Db.getCon().prepareStatement(Query.removeCustomer);
      pst.setString(1, customer.getMobile());
      result = pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      result = 0;
    } finally {
      try {
        Db.getCon().close();
        pst.close();
      } catch (ClassNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    return result;
  }

}


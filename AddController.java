package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ICustomer;
import dao.CustomerImpl;
import model.Customer;

@WebServlet("/add")
public class AddController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String name = request.getParameter("name");
		String mobile = request.getParameter("mobile");
		String amount = request.getParameter("amount");
		 
		 ICustomer acImpl = new CustomerImpl();
		 Customer ac = new Customer();
		 
		ac.setName(name);
	    ac.setMobile(mobile);
	    ac.setAmount(amount);
		int result = acImpl.addCustomer(ac);
		if (result == 1) {
			HttpSession session = request.getSession(false);
			request.setAttribute("msg", name);

			request.getRequestDispatcher("success.jsp").forward(request, response);
		} else {
			request.setAttribute("error", "Patient details are not added successfully!!!");
			request.getRequestDispatcher("add customer.jsp").forward(request, response);
		}

	}
}

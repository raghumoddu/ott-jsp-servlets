package model;
import java.sql.Date;

public class Customer {
	  private String name;
	  private String mobile;
	  private String amount;
	  private Date validupto;

	  public Customer() {
	    // TODO Auto-generated constructor stub
	  }

	  public Customer(String name, String mobile, String amount, Date validupto) {
		super();
		this.name = name;
		this.mobile = mobile;
		this.amount = amount;
		this.validupto = validupto;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Date getValidupto() {
		return validupto;
	}

	public void setValidupto(Date validupto) {
		this.validupto = validupto;
	}

	@Override
	public String toString() {
		return "Customer [name=" + name + ", mobile=" + mobile + ", amount=" + amount + ", validupto=" + validupto + "]";
	}



	 
	}


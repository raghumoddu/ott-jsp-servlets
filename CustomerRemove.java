package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ICustomer;
import dao.CustomerImpl;
import model.Customer;


@WebServlet("/remove")
public class CustomerRemove extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mobile = request.getParameter("mobile");
		ICustomer acImpl = new CustomerImpl();
		Customer ac = new Customer();
	    ac.setMobile(mobile);
	    int result = acImpl.removeCustomer(ac);
	    if (result == 1) {
			

		request.getRequestDispatcher("remove customer.jsp").forward(request, response);
		} else {
			request.setAttribute("error", "customer details removed successfully!!!");
			request.getRequestDispatcher("remove.jsp").forward(request, response);
		}
	}
}

package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ICustomer;
import dao.CustomerImpl;
import model.Customer;

@WebServlet("/update")
public class EditCustomerController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String mobile = request.getParameter("mobile");

		ICustomer acImpl = new CustomerImpl();
		Customer ac = new Customer();
		ac.setMobile(mobile);
	
		int result = acImpl.editCustomer(ac);
		if (result == 1) {
			HttpSession session = request.getSession(false);
			request.setAttribute("msg", mobile);

			request.getRequestDispatcher("updated.jsp").forward(request, response);
		} else {
			request.setAttribute("error", "Patient details are not updated successfully!!!");
			request.getRequestDispatcher("update.jsp").forward(request, response);
		}

	}

}

package dao;

import java.util.List;

import model.Customer;

public interface ICustomer {
	 public int addCustomer(Customer customer);

	  public List<Customer> viewCustomer();

	  public int editCustomer(Customer customer);

	  public int removeCustomer(Customer customer);

}
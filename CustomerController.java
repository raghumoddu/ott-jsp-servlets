package controller;

import java.sql.Date;
import java.util.List;

import dao.CustomerImpl;
import dao.ICustomer;
import model.Customer;

public class CustomerController {
  ICustomer cImpl = new CustomerImpl();

  public int addCustomer(String name,String mobile,String amount, Date validupto) {
    Customer customer = new Customer(name, mobile, amount, validupto);
    return cImpl.addCustomer(customer);
  }

  public List<Customer> viewCustomer() {
    return cImpl.viewCustomer();
  }

  public int editCustomer(String name,String mobile,String amount, Date validupto) {
    Customer customer = new Customer(name, mobile, amount, validupto);
    return cImpl.editCustomer(customer);
  }

  public int removeCustomer(String name) {
	  Customer customer = new Customer();
    customer.setName(name);
    return cImpl.removeCustomer(customer);
  }

}


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2 id="r" align="center"> Add customer details</h2>
<hr/>
<span${"error"}></span>
<form action="add" method="post">
<table align="center" cellspacing="10">
<tr><td>Customer name:</td><td><input type="text" name="name" placeholder="enter name" autocomplete="off"/></td></tr>
<tr><td>Mobile:</td><td><input type="text" name="mobile" placeholder="enter number" autocomplete="off"/></td></tr>
<tr><td>Amount:</td><td><input type="text" name="amount"  placeholder="enter amount" autocomplete="off"/></td></tr>
<tr><td><input type="submit" value="submit" align="center"/></td></tr>
<tr><td><input type="reset" value="cancel" align="center"/></td></tr>
</table>
</form>
<a href="logout.html">Logout</a>

</body>
</html>